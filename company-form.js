(function() {
	'use strict'
	var DEFAULT_STATE_KEY = 'state'
	var DEFAULT_CITY_KEY = 'city'
	var DEFAULT_NEIGHBORHOOD_KEY = 'neighborhood'
	var DEFAULT_STREET_KEY = 'street'

	angular.module('findi.companyForm', ['ionic', 'formlyIonic', 'ngMask',
			'fixMaskInput', 'ionicUpload'
		])
		.component('companyForm', {
			template: '<form name="{{$ctrl.opts.formName}}" novalidate="novalidate" ng-submit="$ctrl.opts.submit($ctrl.ngModel)">' +
				'<formly-form model="$ctrl.ngModel" fields="$ctrl.opts.fields"></formly-form>' +
				'<input class="findi-btn" type="submit" value="{{$ctrl.opts.submitButtonText}}"/>' +
				'</form>',
			controller: userFormController,
			controllerAs: '$ctrl',
			bindings: {
				ngModel: '=',
				options: '='
			}
		})

	function userFormController($scope, formlyConfig, $http, $ionicLoading) {
		var vm = this
		var optionsParams = angular.copy(vm.options) || {}
		vm.opts = _buildOpts(vm, optionsParams, $http, $ionicLoading)

		_createFormlyTypes(formlyConfig, optionsParams)
	}

	var _createFormlyTypes = function(formlyConfig, optionsParams) {
		var formlyTypes = formlyConfig.getTypes()

		if (!formlyTypes['stacked-textarea']) {
			formlyConfig.setType({
				name: 'stacked-textarea',
				template: '<label class="item item-input item-stacked-label">' +
					'<span class="input-label" aria-label="{{options.templateOptions.label}}"' +
					'id="stacked-textarea">{{options.templateOptions.label}}</span>' +
					'<textarea rows="{{options.templateOptions.row || 5}}"' +
					'placeholder="{{options.templateOptions.placeholder}}" ng-model="model[options.key]"' +
					'id="stacked-textarea" name="stacked-textarea"' +
					'formly-custom-validation="options.validators">' +
					'</textarea>' +
					'</label>'
			})
		}

		if (!formlyTypes.photo) {
			var configFields = optionsParams.configFields || {}
			var configFieldPhoto = configFields.photo || {}

			formlyConfig.setType({
				name: 'photo',
				template: '<ionic-upload ng-model="model[options.key]"' +
					'options="options.templateOptions.config" template-url="' +
					configFieldPhoto.templateUrl +
					'" formly-custom-validation="options.validators"></ionic-upload>'
			})
		}
	}

	var _buildOpts = function(vm, optionsParams, $http, $ionicLoading) {
		var opts = {}
		opts.formName = "$ctrl.registerCompanyForm"
		opts.configFields = optionsParams.configFields || {}
		opts.submitButtonText = optionsParams.submitButtonText || 'Continuar'
		opts.fields = _getFields(opts.configFields, $http, $ionicLoading)
		opts.submit = function(data) {
			optionsParams.submit(data, vm.registerCompanyForm)
		}

		return opts
	}

	var _buildConfigCep = function(configFields) {
		var config = configFields.cep || {}

		if (configFields.state && configFields.state.key) config.stateModel =
			configFields.state.key
		if (configFields.city && configFields.city.key) config.cityModel =
			configFields.city.key
		if (configFields.neighborhood && configFields.neighborhood.key) config.neighborhoodModel =
			configFields.neighborhood.key
		if (configFields.street && configFields.street.key) config.streetModel =
			configFields.street.key

		return config
	}

	var _getFields = function(configFields, $http, $ionicLoading) {
		var array = []
		var configFieldPhoto = configFields.photo || {}
		var configFieldFantasyName = configFields.fantasyName || {}
		var configFieldComporateName = configFields.corporateName || {}
		var configCnpjField = configFields.cnpj || {}
		var configCepField = _buildConfigCep(configFields)
		var configFieldState = configFields.state || {}
		var configFieldCity = configFields.city || {}
		var configFieldNeighborhood = configFields.neighborhood || {}
		var configFieldStreet = configFields.street || {}
		var configFieldNumber = configFields.number || {}
		var configFieldComplement = configFields.complement || {}
		var configFieldDescription = configFields.description || {}
		var configFieldVideoUrl = configFields.videoUrl || {}

		if (!configFieldPhoto.hide) array.push(_getPhotoField(configFieldPhoto))
		if (!configFieldFantasyName.hide) array.push(_getFantasyNameField(
			configFieldFantasyName))
		if (!configFieldComporateName.hide) array.push(_getCorporateNameField(
			configFieldComporateName))
		if (!configCnpjField.hide) array.push(_getCnpjField(configCnpjField))
		if (!configFieldVideoUrl.hide) array.push(_getVideoUrlField(
			configFieldVideoUrl))
		if (!configFieldDescription.hide) array.push(_getDescriptionField(
			configFieldDescription))
		if (!configCepField.hide) array.push(_getCepField(configCepField, $http,
			$ionicLoading))
		if (!configFieldState.hide) array.push(_getStateField(configFieldState))
		if (!configFieldCity.hide) array.push(_getCityField(configFieldCity))
		if (!configFieldNeighborhood.hide) array.push(_getNeighborhoodField(
			configFieldNeighborhood))
		if (!configFieldStreet.hide) array.push(_getStreetField(configFieldStreet))
		if (!configFieldNumber.hide) array.push(_getNumberField(configFieldNumber))
		if (!configFieldComplement.hide) array.push(_getComplementField(
			configFieldComplement))

		return array
	}

	var _getFantasyNameField = function(configField) {
		return {
			key: configField.key ? configField.key : "company_name",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'NOME FANTASIA',
				disabled: configField.disabled ? true : false
			}
		}
	}

	var _getPhotoField = function(configField) {
		return {
			key: configField.key ? configField.key : "logo_url",
			type: "photo",
			templateOptions: {
				required: configField.required ? true : false,
				config: configField ? configField : {}
			}
		}
	}

	var _getCorporateNameField = function(configField) {
		return {
			key: configField.key ? configField.key : "trading_name",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'RAZÃO SOCIAL',
				disabled: configField.disabled ? true : false
			}
		}
	}

	var _getCepField = function(configField, $http, $ionicLoading) {
		return {
			key: configField.key ? configField.key : "zip_code",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "tel",
				mask: '99999-?9?9?9?',
				maskClean: true,
				maskInput: '',
				placeholder: configField.placeholder ? configField.placeholder : '',
				label: configField.label ? configField.label : 'CEP',
				disabled: configField.disabled ? true : false
			},
			modelOptions: {
				debounce: configField.debounce ? configField.debounce : 1000
			},
			watcher: {
				listener: function(field, newValue, oldValue, scope, stopWatching) {
					var value = newValue
					resetAddressModel()

					if (value && value.length == 8) {
						_searchBRAddress(value)
						scope.model.typeCep = 'br'
					} else if (value && value.length ==
						5) {
						_searchUSAddress(value)
						scope.model.typeCep = 'us'
					}

					function _searchUSAddress(zipcode) {
						var API_KEY =
							"MreY3EqKOTTC20Sb7Qt8BcDYHBETDrRpS5oWKOHyIBbKC4WJUqSwDwuk89dLB4au";
						var url = "http://www.zipcodeapi.com/rest/" + API_KEY +
							"/info.json/" + zipcode + "/radians";
						var promise = $http.get(url)

						$ionicLoading.show()

						promise.then(function(data) {
							data = data.data
							var stateModel = configField.stateModel || DEFAULT_STATE_KEY
							var cityModel = configField.cityModel || DEFAULT_CITY_KEY

							if (data.city) {
								scope.model[stateModel] = data.state
								scope.model[cityModel] = data.city
							} else {
								var message = configField.zipCodeMessageNotFound ||
									'CEP não encontrado.'
								window.plugins.toast.show(message, 'short', 'bottom',
									null, null);
							}

							$ionicLoading.hide()
						}, function(error) {
							$ionicLoading.hide()
							var message = configField.errorMessageFetchingZipCode ||
								'Erro ao buscar CEP.'
							window.plugins.toast.show(message, 'short', 'bottom',
								null, null)
						})
					}

					function _searchBRAddress(zipCode) {
						var promise = $http.get('http://api.postmon.com.br/v1/cep/' + zipCode)
						$ionicLoading.show()

						promise.then(function(data) {
							data = data.data
							var stateModel = configField.stateModel || DEFAULT_STATE_KEY
							var cityModel = configField.cityModel || DEFAULT_CITY_KEY
							var neighborhoodModel = configField.neighborhoodModel ||
								DEFAULT_NEIGHBORHOOD_KEY
							var streetModel = configField.streetModel || DEFAULT_STREET_KEY

							if (data && data.cidade_info) {
								scope.model[stateModel] = data.estado_info.nome
								scope.model[cityModel] = data.cidade
								scope.model[neighborhoodModel] = data.bairro
								scope.model[streetModel] = data.logradouro
							} else {
								var message = configField.zipCodeMessageNotFound ||
									'CEP não encontrado.'
								window.plugins.toast.show(message, 'short', 'bottom',
									null, null);
							}

							$ionicLoading.hide()
						}, function(error) {
							$ionicLoading.hide()
							var message = configField.errorMessageFetchingZipCode ||
								'Erro ao buscar CEP.'
							window.plugins.toast.show(message, 'short', 'bottom',
								null, null)
						})
					}

					function resetAddressModel() {
						var stateModel = configField.stateModel || DEFAULT_STATE_KEY
						var cityModel = configField.cityModel || DEFAULT_CITY_KEY
						var neighborhoodModel = configField.neighborhoodModel ||
							DEFAULT_NEIGHBORHOOD_KEY
						var streetModel = configField.streetModel || DEFAULT_STREET_KEY
						scope.model[stateModel] = null
						scope.model[cityModel] = null
						scope.model[neighborhoodModel] = null
						scope.model[streetModel] = null
					}
				}
			},
			ngModelAttrs: {
				mask: {
					attribute: 'mask'
				},
				maskClean: {
					attribute: 'mask-clean'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			}
		}
	}

	var _getCityField = function(configField) {
		return {
			key: configField.key ? configField.key : DEFAULT_CITY_KEY,
			type: "stacked-input",
			templateOptions: {
				required: true,
				disabled: true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'CIDADE'
			}
		}
	}

	var _getStateField = function(configField) {
		return {
			key: configField.key ? configField.key : DEFAULT_STATE_KEY,
			type: "stacked-input",
			templateOptions: {
				required: true,
				disabled: true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'ESTADO'
			}
		}
	}

	var _getNeighborhoodField = function(configField) {
		return {
			key: configField.key ? configField.key : DEFAULT_NEIGHBORHOOD_KEY,
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'BAIRRO',
				disabled: configField.disabled ? true : false
			}
		}
	}

	var _getStreetField = function(configField) {
		return {
			key: configField.key ? configField.key : DEFAULT_STREET_KEY,
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'RUA/AV.',
				disabled: configField.disabled ? true : false
			}
		}
	}

	var _getNumberField = function(configField) {
		return {
			key: configField.key ? configField.key : "number",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "number",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'NÚMERO',
				disabled: configField.disabled ? true : false
			}
		}
	}

	var _getComplementField = function(configField) {
		return {
			key: configField.key ? configField.key : "complement",
			type: "stacked-input",
			templateOptions: {
				type: "text",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'COMPLEMENTO',
				disabled: configField.disabled ? true : false
			}
		}
	}

	var _getDescriptionField = function(configField) {
		return {
			key: configField.key ? configField.key : "description",
			type: "stacked-textarea",
			templateOptions: {
				required: configField.required ? configField.required : true,
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'DESCRIÇÃO',
				disabled: configField.disabled ? true : false
			}
		}
	}

	var _getVideoUrlField = function(configField) {
		return {
			key: configField.key ? configField.key : "video_url",
			type: "stacked-input",
			templateOptions: {
				type: "url",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'VÍDEO DO YOUTUBE',
				disabled: configField.disabled ? true : false
			}
		}
	}

	var _getCnpjField = function(configField) {
		return {
			key: configField.key ? configField.key : "cnpj",
			type: "stacked-input",
			id: 'input-cnpj',
			ngModelAttrs: {
				mask: {
					attribute: 'mask'
				},
				maskClean: {
					attribute: 'mask-clean'
				},
				maskInput: {
					attribute: 'fix-mask-input'
				}
			},
			templateOptions: {
				required: true,
				type: "tel",
				mask: '99.999.999/9999-99',
				maskClean: true,
				maskInput: '',
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'CNPJ',
				disabled: configField.disabled ? true : false
			},
			validators: {
				cnpj: {
					expression: function($viewValue, $modelValue) {
						var _isCharactersEquals = function(cnpj) {
							var array = cnpj.split('')
							var isEquals = true

							for (var i = 1; i <= array.length - 1; i++) {
								if (array[i] != array[i - 1]) {
									isEquals = false
									break
								}
							}

							return isEquals
						}

						var _verifyCnpj = function(cnpj, digitToCheck) {
							var array = cnpj.split('')
							var sum = 0
							var j = digitToCheck == 'FIRST_DIGIT' ? 5 : 6
							var length = digitToCheck == 'FIRST_DIGIT' ? 12 : 13
							var comparatorDigit = digitToCheck == 'FIRST_DIGIT' ? 12 : 13

							for (var i = 0; i < length; i++) {
								sum = sum + (parseInt(array[i]) * j)
								j--

								if (j < 2) j = 9
							}


							if ((sum % 11) < 2) {
								if (array[comparatorDigit] != 0) return false
							} else {
								var verifyingDigit = Math.round(11 - (sum % 11))
								if (verifyingDigit != array[comparatorDigit]) return false
							}

							return true
						}

						return /^[0-9]{2}\.[0-9]{3}\.[0-9]{3}\/[0-9]{4}-[0-9]{2}$/.test(
							$viewValue) && !_isCharactersEquals($modelValue) && _verifyCnpj(
							$modelValue, 'FIRST_DIGIT') && _verifyCnpj($modelValue,
							'SECOND_DIGIT')
					}
				}
			}
		}
	}
})()
