### Dependencies
Ionic
[Angular formly](https://github.com/formly-js/angular-formly)
[ngMask](https://github.com/candreoliveira/ngMask)
[Angular formly ionic](https://github.com/formly-js/angular-formly-templates-ionic)
[fix-mask-input](https://github.com/ivanguimam/fix-mask-input.git)
[upload-photo-component](https://bitbucket.org/findibr/upload-photo-component.git)
[window.plugins.toast](https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin)
```html
<script src="lib/ionic/js/ionic.bundle.js"></script>
<script src="lib/api-check/dist/api-check.min.js"></script>
<script src="lib/angular-formly/dist/formly.min.js"></script>
<script src="lib/angular-formly-templates-ionic/dist/angular-formly-templates-ionic.js"></script>
<script src="lib/company-form/company-form.js"></script>
<script src='lib/ngMask/dist/ngMask.min.js'></script>
<script src="lib/fix-mask-input/fix-mask-input.js"></script>
<script src="lib/upload-photo-component/upload-photo-component.js"></script>
```

### Insert module
```javascript
angular.module('myModule', ['findi.companyForm'])
```

### Use
```html
<company-form ng-model="myModel" options="inputOptions"></company-form>
```
### inputOptions
```javascript
{
	configFields: { // optional
		fantasyName: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		corporateName: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		cnpj: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		cep: { //
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
			zipCodeMessageNotFound: 'STRING' // Zip message not found
			errorMessageFetchingZipCode: 'STRING' // Error message fetching zip code
			debounce: 'INT' // debounce from search address
		},
		state: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		city: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		neighborhood: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		street: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		number: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		complement: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		video_url: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		description: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
		},
		photo: {
			hide: 'BOOLEAN',
			key: 'STRING',
			required: 'BOOLEAN',
			templateUrl: 'STRING',
			titleActionSheet: 'STRING',
			actionSheetCameraText: 'STRING',
			actionSheetGaleryText: 'STRING',
			actionSheetCancelText: 'STRING',
			uploadUrl: 'STRING'
		}
	},
	submit: function(data, form) { // required
		// data: myModel
		// form: form
	},
	submitButtonText: 'STRING' // Text button submit
}
```
